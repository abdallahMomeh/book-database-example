package com.tech_teach.example.bookdatabaseexample.Activities;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tech_teach.example.bookdatabaseexample.AppParameter;
import com.tech_teach.example.bookdatabaseexample.CustomViews.NonScrollListView;
import com.tech_teach.example.bookdatabaseexample.DB.DBHelper;
import com.tech_teach.example.bookdatabaseexample.Models.ReadingSession;
import com.tech_teach.example.bookdatabaseexample.Models.User;
import com.tech_teach.example.bookdatabaseexample.R;
import com.tech_teach.example.bookdatabaseexample.UsersAdapter;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Queue;

public class Mainscreen extends AppCompatActivity {

    AppParameter parameter;
    DBHelper dbHelper;
    List<User> users;
    List<User> newUsers;
    List<ReadingSession>userSessions;
    TextView txtActivityTitle;
    NonScrollListView userList;
    TextView txtPercentageEfta;
    TextView txtPercentage;
    TextView txtUserList;
    TextView txtOrderUserEfta;
    TextView txtOrderUser;
    EditText inputUser;
    UsersAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainscreen);
        parameter = new AppParameter(this);
        userSessions= new ArrayList<>();
        newUsers=new ArrayList<>();
        findViews();
         dbHelper=new DBHelper(this);
        if(!parameter.getBoolean("setDatabase")){
            setDatabase();
        }

      //  aggregationReadPages();

        aggregationReadPages();



        inputUser.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length()>0){
                    if (Integer.parseInt(""+charSequence)>0) {
                        users= dbHelper.getOrderedUser();
                      //  adapter = new UsersAdapter(Mainscreen.this,users);
                     //   userList.setAdapter(adapter);
                        setView(Integer.parseInt(""+charSequence));
                    }
                }else {
                    clearView();
              //      users= new ArrayList<User>();
                   // adapter = new UsersAdapter(Mainscreen.this,users);
                    //userList.setAdapter(adapter);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    void setView(int id){
        for(int i=0;i<newUsers.size();i++){
            if(id==newUsers.get(i).getId()){
                txtOrderUser.setText(""+(i+1));
                txtPercentage.setText(""+((float)newUsers.get(i).getNumPages()/(float)70));
                break;
            }
        }
    }
    void clearView(){
        txtOrderUser.setText("");
        txtPercentage.setText("");
    }

    void findViews(){
        inputUser=(EditText)findViewById(R.id.inputUser);
        userList= (NonScrollListView)findViewById(R.id.userList);
        txtActivityTitle=(TextView)findViewById(R.id.txtActivityTitle);
        txtOrderUserEfta=(TextView)findViewById(R.id.txtOrderUserEfta);
        txtPercentageEfta=(TextView)findViewById(R.id.txtPercentageEfta);
        txtPercentage=(TextView)findViewById(R.id.txtPercentage);
        txtUserList=(TextView)findViewById(R.id.txtUserList);
        txtOrderUser=(TextView)findViewById(R.id.txtOrderUser);

        txtActivityTitle.setTypeface(changeFont(this));
        txtOrderUser.setTypeface(changeFont(this));
        txtOrderUserEfta.setTypeface(changeFont(this));
        txtPercentageEfta.setTypeface(changeFont(this));
        txtPercentage.setTypeface(changeFont(this));
        txtUserList.setTypeface(changeFont(this));

    }

    void setDatabase(){

        Toast.makeText(this, "set database", Toast.LENGTH_SHORT).show();
      /*  dbHelper.insertUser("jane");
        dbHelper.insertUser("ahmed");

        dbHelper.insertReadPages(1,20,1);
        dbHelper.insertReadPages(2,5,2);
        dbHelper.insertReadPages(10,30,1);
        dbHelper.insertReadPages(20,70,2);*/

        dbHelper.insertUser("jane");
        dbHelper.insertUser("ahmed");
        dbHelper.insertUser("ali");

        dbHelper.insertReadPages(1,20,1);
        dbHelper.insertReadPages(10,30,1);
        dbHelper.insertReadPages(2,5,2);
        dbHelper.insertReadPages(20,70,2);
        dbHelper.insertReadPages(30,40,3);
        dbHelper.insertReadPages(35,55,3);

        parameter.setBoolean("setDatabase",true);

    }

    public static Typeface changeFont(Context context){
        return Typeface.createFromAsset(context.getAssets(),"fonts/Cocon® Next Arabic-Light.otf");
    }

    /*void aggregationReadPages(){
        users=dbHelper.getAllUser();
        for (int i=0;i<users.size();i++){

            userSessions=dbHelper.getReadSettions(users.get(i).getId());

            for (int y=0;y<userSessions.size();y++){

                for(int z=0;z<userSessions.size();z++){
                    if(z!=y){

                        if(userSessions.get(z).getFrom()>=userSessions.get(y).getFrom()
                                &&userSessions.get(z).getFrom()<=userSessions.get(y).getTo()
                                &&userSessions.get(z).getTo()>=userSessions.get(y).getTo()){

                            ReadingSession rSession= userSessions.get(z);
                            rSession.setFrom(userSessions.get(y).getTo()+1);

                            userSessions.set(z,rSession);

                        }

                        if(userSessions.get(z).getFrom()>=userSessions.get(y).getFrom()
                                &&userSessions.get(z).getFrom()<=userSessions.get(y).getTo()
                                &&userSessions.get(z).getTo()<=userSessions.get(y).getTo()){

                            ReadingSession rSession= userSessions.get(z);
                            rSession.setFrom(2);
                            rSession.setTo(1);    //   2-1 = -1 // +1 =0

                            userSessions.set(z,rSession);

                        }

                        if(userSessions.get(z).getFrom()<=userSessions.get(y).getFrom()
                                &&userSessions.get(z).getFrom()<=userSessions.get(y).getTo()
                                &&userSessions.get(z).getTo()>=userSessions.get(y).getFrom()
                                &&userSessions.get(z).getTo()<=userSessions.get(y).getTo()){

                            ReadingSession rSession= userSessions.get(z);
                        //    rSession.setFrom(2);
                            rSession.setTo(userSessions.get(y).getFrom()-1);

                            userSessions.set(z,rSession);

                        }
                    }
                }
            }

            Log.d("id:",""+(i+1));
            User u =new User();
            int allReadPaged=0;
            for (int k=0;k<userSessions.size();k++){
                allReadPaged+=(userSessions.get(k).getTo()-userSessions.get(k).getFrom()+1);   // plus 1
              //  Log.d("userSession"+i,""+userSessions.get(k).getTo()+" , "+userSessions.get(k).getFrom());
                Log.d("userSession"+i,""+allReadPaged);

                if(k==(userSessions.size()-1)){
                    u.setNumPages(allReadPaged);
                    u.setUserName(userSessions.get(k).getUserName());
                    u.setId(userSessions.get(k).getUser_id());
                    newUsers.add(u);
                }
                Log.d("userSession"+i,""+u.getNumPages());
            }




        }

        Collections.sort(newUsers, new Comparator<User>() {
            @Override
            public int compare(User user, User t1) {

                return user.getNumPages() > t1.getNumPages() ? -1 : (user.getNumPages() < t1.getNumPages()) ? 1 : 0;
            }
        });

        adapter = new UsersAdapter(Mainscreen.this,newUsers);
        userList.setAdapter(adapter);
    }*/

    void aggregationReadPages(){

        userSessions=dbHelper.getAllReadSettions();
        for(int i=0;i<userSessions.size();i++){

            for (int y=0;y<=i;y++){
                if(i!=0 && i!=y){

                    if(userSessions.get(i).getUser_id()==userSessions.get(y).getUser_id()){

                        if(userSessions.get(i).getFrom()>=userSessions.get(y).getFrom()
                                &&userSessions.get(i).getFrom()<=userSessions.get(y).getTo()
                                &&userSessions.get(i).getTo()>=userSessions.get(y).getTo()){

                            ReadingSession rSession= userSessions.get(i);
                            rSession.setFrom(userSessions.get(y).getTo()+1);

                            userSessions.set(i,rSession);

                        }

                        if(userSessions.get(i).getFrom()>=userSessions.get(y).getFrom()
                                &&userSessions.get(i).getFrom()<=userSessions.get(y).getTo()
                                &&userSessions.get(i).getTo()<=userSessions.get(y).getTo()){

                            ReadingSession rSession= userSessions.get(i);
                            rSession.setFrom(2);
                            rSession.setTo(1);    //   2-1 = -1 // +1 =0

                            userSessions.set(i,rSession);

                        }

                        if(userSessions.get(i).getFrom()<=userSessions.get(y).getFrom()
                                &&userSessions.get(i).getFrom()<=userSessions.get(y).getTo()
                                &&userSessions.get(i).getTo()>=userSessions.get(y).getFrom()
                                &&userSessions.get(i).getTo()<=userSessions.get(y).getTo()){

                            ReadingSession rSession= userSessions.get(i);
                            //    rSession.setFrom(2);
                            rSession.setTo(userSessions.get(y).getFrom()-1);

                            userSessions.set(i,rSession);

                        }

                    }

                }
            }
        }
        int allReadPaged=0;
        for (int k=0;k<userSessions.size();k++){

            User u =new User();

            allReadPaged+=(userSessions.get(k).getTo()-userSessions.get(k).getFrom()+1);   // plus 1
//              Log.d("userSession"+k,""+userSessions.get(k).getTo()+" , "+userSessions.get(k).getFrom());
            Log.d("userSession"+k,""+allReadPaged);

            if(k!=(userSessions.size()-1)){
                if(userSessions.get(k).getUser_id()!=userSessions.get(k+1).getUser_id()){
                    u.setNumPages(allReadPaged);
                    u.setUserName(userSessions.get(k).getUserName());
                    u.setId(userSessions.get(k).getUser_id());
                    newUsers.add(u);
                    allReadPaged=0;
                }
            }else {
                u.setNumPages(allReadPaged);
                u.setUserName(userSessions.get(k).getUserName());
                u.setId(userSessions.get(k).getUser_id());
                newUsers.add(u);
                allReadPaged=0;
            }

        }


        Collections.sort(newUsers, new Comparator<User>() {
            @Override
            public int compare(User user, User t1) {

                return user.getNumPages() > t1.getNumPages() ? -1 : (user.getNumPages() < t1.getNumPages()) ? 1 : 0;
            }
        });

        adapter = new UsersAdapter(Mainscreen.this,newUsers);
        userList.setAdapter(adapter);
    }

}
