package com.tech_teach.example.bookdatabaseexample.Models;

/**
 * Created by abdallah on 4/19/2018.
 */

public class User {
    private int id;
    private String userName;
    private int numPages;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getNumPages() {
        return numPages;
    }

    public void setNumPages(int numPages) {
        this.numPages = numPages;
    }
}
