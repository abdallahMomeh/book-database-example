package com.tech_teach.example.bookdatabaseexample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tech_teach.example.bookdatabaseexample.Activities.Mainscreen;
import com.tech_teach.example.bookdatabaseexample.Models.User;

import java.util.List;

/**
 * Created by abdallah on 4/20/2018.
 */

public class UsersAdapter extends BaseAdapter{

    Context context;
    List<User> users;
    public  UsersAdapter(Context context,List<User> users){
        this.context=context;
        this.users=users;
    }
    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int i) {
        return users.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view == null){
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=layoutInflater.inflate(R.layout.user_item,null);
            holder = new ViewHolder();
            holder.txtUserNum=view.findViewById(R.id.txtUserNum);
            holder.txtUserName=view.findViewById(R.id.txtUserName);
            holder.txtNumPages=view.findViewById(R.id.txtNumPages);

            holder.txtNumPages.setTypeface(Mainscreen.changeFont(context));
            holder.txtUserName.setTypeface(Mainscreen.changeFont(context));
            holder.txtUserNum.setTypeface(Mainscreen.changeFont(context));
            view.setTag(holder);
        }else {
            holder= (ViewHolder) view.getTag();
        }

        holder.txtUserNum.setText("id: "+users.get(i).getId());
        holder.txtUserName.setText("name: "+users.get(i).getUserName());
        holder.txtNumPages.setText("read pages: "+users.get(i).getNumPages());
        return view;
    }

    class ViewHolder {
        TextView txtUserNum;
        TextView txtUserName;
        TextView txtNumPages;
    }
}
