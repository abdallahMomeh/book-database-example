package com.tech_teach.example.bookdatabaseexample;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.SharedPreferences.*;

/**
 * Created by abdallah on 4/20/2018.
 */

public class AppParameter {
    public static final String APP_PREFERENCES = "databaseExamplePrefs";
    SharedPreferences appSettings;
    Editor editor;
    Context context;

    public AppParameter(Context context){
        this.context=context;
    }

    private void openConnection(){
        appSettings = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        editor = appSettings.edit();
    }
    private void closeConnection(){
        appSettings = null;
        editor = null;
    }

    public void setBoolean(String key , boolean value){
        openConnection();
        editor.putBoolean(key,value);
        editor.commit();
        closeConnection();
    }
    public boolean getBoolean(String key){
        boolean result=false;
        openConnection();
        if (appSettings.contains(key)) {
            result =appSettings.getBoolean(key,false);
        }
        closeConnection();
        return result;
    }
}
