package com.tech_teach.example.bookdatabaseexample.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.tech_teach.example.bookdatabaseexample.Models.ReadingSession;
import com.tech_teach.example.bookdatabaseexample.Models.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abdallah on 22/3/2018.
 */

public class DBHelper extends SQLiteOpenHelper {
    Context context;
    public DBHelper(Context context) {
        super(context, "BookExample.db", null, 4);
        this.context=context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table User" +
                        "( id integer primary key, name text );"
        );

        db.execSQL(
                "create table  ReadPages" +
                        "( id integer primary key, fromPage integer, toPage integer, user_id integer );"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS User");
        db.execSQL("DROP TABLE IF EXISTS ReadPages");
        onCreate(db);
    }

    public boolean insertUser (String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        db.insert("User", null, contentValues);
        return true;
    }


    public boolean insertReadPages (int from,int to,int userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("fromPage", from);
        contentValues.put("toPage", to);
        contentValues.put("user_id", userId);
        db.insert("ReadPages", null, contentValues);
        return true;
    }



    public String getUser(int userId){

        SQLiteDatabase db = getReadableDatabase();
        String userName="";
        Cursor res= db.rawQuery("select * from User where id="+userId+"",null);
        if(res.getCount()>0){
            res.moveToFirst();
            userName=res.getString(1);
            res.close();
            db.close();
            return userName;
        }else {
            return "not found";
        }

    }

    public List<User> getAllUser(){
        List<User>users=new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor res= db.rawQuery("select * from User",null);
        if(res.getCount()>0){
            res.moveToFirst();
            for(int i=0;i<res.getCount();i++){
                User user=new User();
                user.setId(res.getInt(0));
                user.setUserName(res.getString(1));
                users.add(user);
                res.moveToNext();
            }

            res.close();
            db.close();
            return users;
        }else {
            return users;
        }

    }


    public int getReadPages(int userId){

        SQLiteDatabase db = getReadableDatabase();
        int  from=0;
        int  to=0;
        int  all=0;
        Cursor res= db.rawQuery("select * from ReadPages where user_id="+userId+"",null);
        if(res.getCount()>0){
            for(int i=0;i<res.getCount();i++){


                from=res.getInt(1);
                to=res.getInt(2);
                all+=((to-from)+1);
                res.moveToNext();
            }
            res.close();
            db.close();

            return all;
        }else {
            return 0;
        }

    }


    public List<ReadingSession> getReadSettions(int userId){
        List<ReadingSession> readingSessions=new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor res= db.rawQuery("select User.name , ReadPages.fromPage , ReadPages.toPage , ReadPages.user_id " +
                "from User inner join ReadPages on User.id=ReadPages.user_id where ReadPages.user_id="+userId+"",null);
        Log.d("count_size",""+res.getCount());
        if(res.getCount()>0){
            res.moveToFirst();
            for(int i=0;i<res.getCount();i++){

                ReadingSession session=new ReadingSession();
                session.setUserName(res.getString(0));
                session.setFrom(res.getInt(1));
                session.setTo(res.getInt(2));
                session.setUser_id(res.getInt(3));
                readingSessions.add(session);
                res.moveToNext();
            }
            res.close();
            db.close();

            return readingSessions;
        }else {
            return readingSessions;
        }

    }



    public List<ReadingSession> getAllReadSettions(){
        List<ReadingSession> readingSessions=new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor res= db.rawQuery("select User.name , ReadPages.fromPage , ReadPages.toPage , ReadPages.user_id " +
                "from User inner join ReadPages on User.id=ReadPages.user_id",null);
        Log.d("count_size",""+res.getCount());
        if(res.getCount()>0){
            res.moveToFirst();
            for(int i=0;i<res.getCount();i++){

                ReadingSession session=new ReadingSession();
                session.setUserName(res.getString(0));
                session.setFrom(res.getInt(1));
                session.setTo(res.getInt(2));
                session.setUser_id(res.getInt(3));
                readingSessions.add(session);
                res.moveToNext();
            }
            res.close();
            db.close();

            return readingSessions;
        }else {
            return readingSessions;
        }

    }


    public String getCountPage(){


        SQLiteDatabase db = getReadableDatabase();
        int  from=0;
        int  to=0;
        int  all=0;
        String userName="";
        Cursor res= db.rawQuery("select User.name , SUM(ReadPages.toPage-ReadPages.fromPage) from User inner join ReadPages " +
                "on User.id=ReadPages.user_id group by User.name" +
                " order by  SUM(ReadPages.toPage-ReadPages.fromPage) ",null);
        if(res.getCount()>0){
            //   for(int i=0;i<res.getCount();i++){

            Toast.makeText(context, ""+res.getCount(), Toast.LENGTH_SHORT).show();
            res.moveToFirst();
            userName=res.getString(0);
            Toast.makeText(context, ""+res.getInt(1), Toast.LENGTH_SHORT).show();
            res.moveToNext();
            userName=res.getString(0);
            Toast.makeText(context, ""+res.getInt(1), Toast.LENGTH_SHORT).show();
               /* from=res.getInt(1);
                to=res.getInt(2);
                all+=((to-from)+1);
                res.moveToNext();*/
            //}
            res.close();
            db.close();

            return userName;
        }else {
            return userName;
        }

    }

    public List<User> getOrderedUser(){
        List<User>users=new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        String userName="";
        Cursor res= db.rawQuery("select User.id , User.name ,SUM(toPage-fromPage)  from User inner join ReadPages " +
                "on User.id=ReadPages.user_id group by User.name" +
                " order by  SUM(toPage-fromPage) DESC",null);
        if(res.getCount()>0){
            res.moveToFirst();
            for(int i=0;i<res.getCount();i++){

                User user= new User();
                user.setId(res.getInt(0));
                user.setUserName(res.getString(1));
                user.setNumPages(res.getInt(2));
                users.add(user);
                res.moveToNext();

            }
            res.close();
            db.close();

            return users;
        }else {
            return users;
        }

    }

}

